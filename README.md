# Unipolární "dual-band" antény: Není prut jako prut

**Slyšeli jste někdy o tom, že pro lepší zisk je důležité ruční vysílačku správně
chytnout a tím dát anténě vhodnou protiváhu? Vyplatí se koupě renomovaných
antén namísto ekonomicky přívětivých kopií? Existují dual-band antény vhodné
pro česká pásma? Pevně doufám a věřím, že odpovědi naleznete v následujícím
článku.**

Pobíhání po kopci při hledání dobrého místa odkud je čistě slyšet několik
stovek kilometrů vzdálený kolega je i mou oblíbenou kratochvílí. Z pracovních
důvodů jsem se ale ocitl na placce, kde jsem rád, když jsem v kladné nadmořské
výšce. Slovo kopec je tu prázdný pojem. Jelikož se naštěstí krom běhání po
kopcích hrabu i v instrumentaci, přináším Vám srovnání unipolárních antén z
mého šuplíku se zřetelem na česká veřejná pásma. Doufám, že se bude líbit.


### Co budeme měřit?
Vcelku pestrou paletu antén na ručky (obrázek 1), všechno víceméně 1/4 a 1/2vlnné (vzhledem k PMR) unipolární antény. 


<img src="https://gitlab.science.ru.nl/jzelenka/vswr_clanek_2021_01/-/raw/master/fotky/antenny_merka.jpg"  width="500">

**Obrázek 1.** *testované antény: 5 ks čínské (?) kopie Na-771, 1 ks Retevis (není jisté že se jedná o originální anténu), 3 ks originálních Nagoya NA-771, 2 ks originálních Nagoya NA-701, 1 ks staré antény k Baofengu UV-5R, 2 ks novějších antén k Baofengu UV-5R. Na vrcholu kupy antén SWR měrka - "Vector antenna analyzer FA-VA5".*


### Čím budeme měřit?
Po delší úvaze jsem se rozhodl si udělat radost a pořídil jsem si SWR měrku (Obrázek 1). Bohužel s ní nepokrývám pásmo wifi a nemám výsledky ověřené z nezávislého SWR metru. Na druhou stranu technika by měla fungovat dobře a stroj jsem kalibroval těsně před započetím měření a v průběhu měření jsem kalibraci kontroloval. Anténu jsem připojil k SWR metru redukcí BNC/SMA a provedl jsem kontrolní měření "bez zátěže" a s 50Ω kalibrovanou zátěží, která byla připojena přes další redukci, tentokrát SMA/BNC (graf 1a). Při testování "bez zátěže" (graf 1b, pozor, je zde vyobrazena reflekance, nikoliv VSWR) jsem zaznamenal zajímavý efekt, kdy v pásmu +400MHz bylo víc odražené vlny, než při sundané redukci. To znamená, že SWR v tomto pásmu může vypadat nepatrně horší, než ve skutečnosti je. 


<img src="https://gitlab.science.ru.nl/jzelenka/vswr_clanek_2021_01/-/raw/master/figs_new/swr_reduction/swr_redukce.png"  width="500" ><br>
<img src="https://gitlab.science.ru.nl/jzelenka/vswr_clanek_2021_01/-/raw/master/figs_new/swr_reduction/open.png"  width="500" >

**Graf 1.** *Vliv redukcí na hodnotu SWR je u 50Ω kalibrované zátěže vcelku zanedbatelný. Pro hodnoty blízké ideálnímu SWR by tedy měl být vliv redukce zanedbatelný. Při velmi vysokých SWR (měřeno otevřenou referencí) je odchylka již patrná.*

### Metodika
Jelikož chci článek zpřístupnit i začínajícím hobíkům, tak pár slov v kostce o VSWR (SWR) a refklečním koeficientu: je to číslo, které udává, kolik ze signálu vpraveného do antény se vyzáří, a kolik se odrazí zpět do vysílačky. Toto číslo tedy udává, jak snadno anténa přijímá a vysílá vlny. Když je SWR nízké, anténa funguje v daném pásmu dobře. SWR 1 znamená, že veškerá energie se vyzáří, SWR 3 znamená, že půlka energie se odrazí. Čím vyšší SWR, tím hůř. Gamma je reflekční koeficient ze kterého se VSWR počítá a vyjadřuje čistě kolik vlny se odrazí zpět do vysílače. Gamma = 0 znamená, že všechnu energii pohltila anténa (a nejspíš i vyzářila, tedy velmi dobrá anténa). Gamma = 1 znamená, že z antény nebylo vyzářeno nic a vše se odrazilo zpět do vysílačky (nepoužitelná anténa).

Mám již zkušenosti s tím, že úchop má vliv na charakteristiku antény. Proto jsem prováděl měření s úchopem "natěsno" (obrázek 2a), "v ruce" (obrázek 2b) a "volně" (obrázek 2c). Měření tedy proběhla v podmínkách rozhodně neideálních, ale pro získání základní představy o charakteristice jednotlivých antén snad dostačujících. Vliv úchopu je jasně vidět na grafu 2. Je patrné, že s těsnějším úchopem dochází k rozšíření pásem s dobrým VSWR. Prováděl jsem vždy 2 měření. Na průběh grafu má vliv nejenom úchop, ale i držení těla. Tento vliv je patrný většinou až vyšších SWR a u "dna" (použitelné frekvence antény) jsou až na vyjímky rozdíly vcelku malé.

<img src="https://gitlab.science.ru.nl/jzelenka/vswr_clanek_2021_01/-/raw/master/fotky/uchopy.jpg"  width="500" >

**Obrázek 2.** *Úchop a) "natěsno" b) "v ruce" c) "volně."*

<img src="https://gitlab.science.ru.nl/jzelenka/vswr_clanek_2021_01/-/raw/master/figs_new/uchopy_nag/uchop_771.png"  width="500" ><br>
<img src="https://gitlab.science.ru.nl/jzelenka/vswr_clanek_2021_01/-/raw/master/figs_new/uchopy_nag/uchop_701.png"  width="500" >

**Graf 2.** *a) Vliv úchopu na SWR charakteristiku originální antény NA-771 b) vliv úchopu na SWR charakteristiku originální antény NA-701. Z grafů je patrné, že pevnější úchop rozšiřuje v zájmových frekvencích pásmo s nízkým SWR a zároveň posouvá minima SWR směrem k nižším frekvencím (delším vlnám).*

### Antény

---

##### Nagoya NA-771 čínská (?) kopie

<img src="https://gitlab.science.ru.nl/jzelenka/vswr_clanek_2021_01/-/raw/master/fotky/na771_copy.jpg"  width="500" >

**Obrázek 3.** *Vcelku častá kopie NA-771.*

Antény jsem pořídil v jednom balení se slevou a s vidinou, že je trochu zastřihnu a tím doladím ze 144 MHz na 172 MHz. Jak je z grafu (Graf 3) patrné, anténa pro tento účel není vhodná ze dvou důvodů: nemá dobré přizpůsobení, a nebyla lazena na 144 MHz.

<img src="https://gitlab.science.ru.nl/jzelenka/vswr_clanek_2021_01/-/raw/master/figs_new/chinag771/chinag_compar.png"  width="500" ><br>
<img src="https://gitlab.science.ru.nl/jzelenka/vswr_clanek_2021_01/-/raw/master/figs_new/chinag771/chinag_vs_nag.png"  width="500" >

**Graf 3.** *a) Průběh SWR u vzorku pěti kopií antény NA-771, nestabilní měření při úchopu v ruce jsou způsobena změnou úchopu b) srovnání průběhu kopie antény NA-771 s originální NA-771. Z grafu a) je patrné, že výstup čínské (?) fabriky je vesměs homogenní. Graf b) ukazuje, že zatímco v 2m pásmu není kopie o moc horší, v pásmu 446 je rozdíl více nežli patrný.  Pro graf b) byla použita data naměřená "volně"*

---


##### Originální Nagoya NA-771

<img src="https://gitlab.science.ru.nl/jzelenka/vswr_clanek_2021_01/-/raw/master/fotky/na771_orig.jpg"  width="500" >

**Obrázek 4.** *Originální Nagoya NA-771.*

Anténa byla objednaná od německého dodavatele doporučeného přímo firmou Nagoya. Opět marná snaha o získání dobré výchozí antény na přelazení na 172 MHz. Na druhou stranu na 172MHz vypadá použitelně již v základu.

<img src="https://gitlab.science.ru.nl/jzelenka/vswr_clanek_2021_01/-/raw/master/figs_new/nag771/771nag_compar.png"  width="500" >

**Graf 4.** *Originální antény NA-771 jsou lepší než jejich čínské (?) kopie, především u 446. Odlehlá měření jsou provedena na anténě s konektorem "samec"*

---

##### Anténa Retevis

<img src="https://gitlab.science.ru.nl/jzelenka/vswr_clanek_2021_01/-/raw/master/fotky/retevis.jpg"  width="500" >

**Obrázek 5.** *Retevis RT-771. Možná originální, možná kopie.*

Anténu jsem objednal, když jsem se uklikl při objednávce čínských (?) kopií NA-771.

<img src="https://gitlab.science.ru.nl/jzelenka/vswr_clanek_2021_01/-/raw/master/figs_new/retevis/retevis.png"  width="500" >

**Graf 5.** *anténa Retevis (?) vypadá, že bude opravdu použitelná pro SD2M (172MHz).*

---

##### Originální Nagoya NA-701

<img src="https://gitlab.science.ru.nl/jzelenka/vswr_clanek_2021_01/-/raw/master/fotky/na701.jpg"  width="500" >

**Obrázek 6.** *Originální Nagoya NA-701.*

Velmi ostrý "zářez" na 144 MHz, PMR pásmo nevypadá úplně dobře.

<img src="https://gitlab.science.ru.nl/jzelenka/vswr_clanek_2021_01/-/raw/master/figs_new/nag701/701nag.png"  width="500" >

**Graf 6.** *Nagoya Na-701 má nádherné VSWR, bohužel ale na špatném místě pro české sdílené pásmo.*

---

##### Anténa dodávaná k ručce Baofeng UV-5R

<img src="https://gitlab.science.ru.nl/jzelenka/vswr_clanek_2021_01/-/raw/master/fotky/bafold.jpg"  width="500" >

**Obrázek 7.** *Starší model antény dodávané k ruční radiocihle Baofeng UV-5R.*

Originální anténa od baofengu předvedla překvapivě dobrý výstup. Co jsem slyšel, tak tyto antény trpí na poškození přizpůsobení v případě nešetrného zacházení.

<img src="https://gitlab.science.ru.nl/jzelenka/vswr_clanek_2021_01/-/raw/master/figs_new/bafold/oldfeng.png"  width="500" >

**Graf 7.** *Starší anténa k baofengu je očividně vcelku použitelná pro PMR.*

---

##### Anténa dodávaná k ručce Baofeng UV-5R2

<img src="https://gitlab.science.ru.nl/jzelenka/vswr_clanek_2021_01/-/raw/master/fotky/bafnew.jpg"  width="500" >

**Obrázek 8.** *Novější model antény dodávané k ruční radiocihle Baofeng UV-5R.*

Jen vědět, který úchop je ten správný...

<img src="https://gitlab.science.ru.nl/jzelenka/vswr_clanek_2021_01/-/raw/master/figs_new/bafnew/bafnew.png"  width="500" >

**Graf 8.** *Novější anténa k Baofengu má velmi zajímavý průběh v PMR pásmu. Pokud se tedy dá při normálním využití použít úchyt odpovídající výstupům "natěsno." Výstup fabriky není homogenní (dvě sady čar v PMR pásmu). Když se vysílačka dobře uchopí, možná by mohla být anténa využitelná pro drážní frekvence (oranžová čerchovaná čára v 2M pásmu).*

---


### Závěr
Bohužel jsem neměl k dispozici jiný SWR metr, či kalibrační sadu pro SMA. Při porovnání s jinými webovými zdroji je ale můj výstup vcelku srovnatelný.[^fn1]<sup>,</sup>[^fn2] Unipolární antény jsou známé tím, že je složité je změřit správným způsobem.[^fn1]<sup>,</sup>[^fn3] Řešením je vyběhnout na kopec a zjistit, jestli naměřené výsledky odpovídají vlastnostem při reálném provozu v SD2M, SD70 a PMR (Dx) pásmech.

Je známou poučkou, že při změně úchopu dochází ke změně protiváhy. Měření ukázala, že vliv úchopu je podstatný. Jinak řečeno: charakteristika antén značně závisí na úchopu ručky/unipolární antény. V průběhu testu jsem si několikrát všiml jasné reakce na změnu nejenom úchopu, ale i polohy ruky, případně vzdálenosti měrky od mého těla.

Pokud mám věřit výsledkům měření, tak pro české SD2M pásmo vypadá lákavě anténa v testu označovaná jako Retevis (těžko říct jestli se jedná o originál, či kopii) a originální antény NA-771. Použitelné na SD2M jsou taktéž kopie NA-771 (originál je poněkud lepší, kopie NA-771 není žádná výhra, ale lepší než drátem do.. nebo než NA-701). Anténa NA701 vypadá jako kvalitní anténa určená na 144MHz, ovšem na SD2M to asi opravdu není "to pravé." Do budoucna by bylo zajímavé porovnat příjem na Retevisu, jehož charakteristiky se s úchopem tolik nemění, s např. anténou dodávanou běžně k UV-5R2. Přímé porovnání "za provozu" v polních podmínkách by mohlo dát odpověď na otázku, který úchop odpovídá reálnému použití. Výhledově plánuji jednu z čínských (?) antén NA771 podrobit destruktivní analýze. Ale o tom až zase příště, tenhle článek je už dlouhý až až...

73 Jenda Strašecí

*Článek vznikl z mé osobní iniciativy, kdy jsem si chtěl udělat pořádek v šuplíku a zjistit, co je při cestě na kopec zbytečnou zátěží. Nebyl jsem (bohužel) sponzorován žádným výrobcem, prodejcem či poskytovatelem zařízení zmíněných v tomto článku. Surová data jsou dostupná na adrese:* https://gitlab.science.ru.nl/jzelenka/vswr_clanek_2021_01/-/tree/master/rawdata/ *Článek neberte jako nákupní doporučení! Není jisté, jak moc odpovídají podmínky měření reálnému provozu.*

### Citace

[^fn1]: https://www.reddit.com/r/Baofeng/comments/jaaz2v/stock_vs_na771_antenna/

[^fn2]: https://forums.qrz.com/index.php?threads/nagoya-na-701-ht-antenna-results.402199/

[^fn3]: https://forums.radioreference.com/threads/trying-to-find-out-how-to-properly-measure-antenna-swr-and-watt-readings.396105/



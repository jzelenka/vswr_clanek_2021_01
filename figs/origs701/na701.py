#!/usr/bin/env python3
import sys
sys.path.append('/home/yan/playground/python/vswrview')
from vswrview_complex import main



if __name__ == "__main__":
    index = ""
    pathprefix = '~/documents/pmr/measurements/'+\
                 '2021_01_09_nagoya_chinagoya_retevis_pruty/rawdata/'
    files = ['nagoya_t.s1p', 'nagoya_t2.s1p', 'na701_bag_t1.s1p', 'na701_bag_t2.s1p',
             'nagoya_l.s1p', 'nagoya_l2.s1p', 'na701_bag_l1.s1p', 'na701_bag_l2.s1p', 
             'nagoya_f.s1p', 'nagoya_f2.s1p', 'na701_bag_f1.s1p', 'na701_bag_f2.s1p']
    styles = [{'color': (0.3, 0, 0.5, 1), 'ls': '-', 'label': "v dlani"},
              {'color': (0.3, 0, 0.5, 1), 'ls': '-'},
              {'color': (0.3, 0, 0.5, 1), 'ls': '-'},
              {'color': (0.3, 0, 0.5, 1), 'ls': '-'},
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.', 'label': "v prstech"},
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.'},
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.'},
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.'},
              {'color': (0.5, 0.5, 0.5, 1), 'ls': '-', 'label': "volně"},
              {'color': (0.5, 0.5, 0.5, 1), 'ls': '-'},
              {'color': (0.5, 0.5, 0.5, 1), 'ls': '-'},
              {'color': (0.5, 0.5, 0.5, 1), 'ls': '-'}]
    outname = 'na701'
    main(pathprefix, files, styles, outname, index)

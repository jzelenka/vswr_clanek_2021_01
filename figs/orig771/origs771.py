#!/usr/bin/env python3
import sys
sys.path.append('/home/yan/playground/python/vswrview')
from vswrview_complex import main



if __name__ == "__main__":
    index = ""
    pathprefix = '~/documents/pmr/measurements/'+\
                 '2021_01_09_nagoya_chinagoya_retevis_pruty/rawdata/na771_'
    files = ['1_h.s1p', '1_h2.s1p', '2_t1.s1p', '2_t2.s1p', 'male_t1.s1p', 'male_t2.s1p', '1_l.s1p', '1_l2.s1p', '2_l1.s1p', '2_l2.s1p', 'male_l1.s1p', 'male_l2.s1p', '1_f.s1p', '1_f2.s1p', '2_f1.s1p', '2_f2_hanging.s1p', 'male_f.s1p', 'male_f2.s1p']
    styles = [{'color': (0.3, 0, 0.5, 1), 'ls': '-', 'label': "v dlani"},
              {'color': (0.3, 0, 0.5, 1), 'ls': '-'}, 
              {'color': (0.3, 0, 0.5, 1), 'ls': '-'}, 
              {'color': (0.3, 0, 0.5, 1), 'ls': '-'}, 
              {'color': (0.3, 0, 0.5, 1), 'ls': '-'}, 
              {'color': (0.3, 0, 0.5, 1), 'ls': '-'}, 
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.', 'label': "v prstech"},
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.'},
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.'},
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.'},
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.'},
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.'},
              {'color': (0.5, 0.5, 0.5, 1), 'ls': '-', 'label': "volně"},
              {'color': (0.5, 0.5, 0.5, 1), 'ls': '-'},
              {'color': (0.5, 0.5, 0.5, 1), 'ls': '-'},
              {'color': (0.5, 0.5, 0.5, 1), 'ls': '-'},
              {'color': (0.5, 0.5, 0.5, 1), 'ls': '-'},
              {'color': (0.5, 0.5, 0.5, 1), 'ls': '-'}]
    outname = 'origs771'
    main(pathprefix, files, styles, outname, index)

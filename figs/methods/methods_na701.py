#!/usr/bin/env python3
import sys
sys.path.append('/home/yan/playground/python/vswrview')
from vswrview_complex import main



if __name__ == "__main__":
    index = "b)"
    pathprefix = '~/documents/pmr/measurements/'+\
                 '2021_01_09_nagoya_chinagoya_retevis_pruty/rawdata/na701_bag_'
    files = ['t1.s1p', 't2.s1p', 'l1.s1p', 'l2.s1p', 'f1.s1p', 'f2.s1p']
    styles = [{'color': (0.3, 0, 0.5, 1), 'ls': '-', 'label': "v dlani"},
              {'color': (0.3, 0, 0.5, 1), 'ls': '-'},
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.', 'label': "v prstech"},
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.'},
              {'color': (0.5, 0.5, 0.5, 1), 'ls': '-', 'label': "volně"},
              {'color': (0.5, 0.5, 0.5, 1), 'ls': '-'}]
    outname = 'methods_na701_bag'
    main(pathprefix, files, styles, outname, index)

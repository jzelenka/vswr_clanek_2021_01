#!/usr/bin/env python3
import sys
sys.path.append('/home/yan/playground/python/vswrview')
from vswrview_complex import main



if __name__ == "__main__":
    index = ""
    pathprefix = '~/documents/pmr/measurements/'+\
                 '2021_01_09_nagoya_chinagoya_retevis_pruty/rawdata/baofeng_new'
    files = ['_h.s1p', '_h2.s1p', '_h3-afternew2.s1p', '2_t.s1p', '2_t2.s1p', 
             '_l.s1p', '_l2.s1p', '2_l.s1p', '2_l2.s1p', 
             '_f.s1p', '_f2.s1p', '2_f.s1p', '2_f2.s1p']
    styles = [{'color': (0.3, 0, 0.5, 1), 'ls': '-', 'label': "v dlani"},
              {'color': (0.3, 0, 0.5, 1), 'ls': '-'},
              {'color': (0.3, 0, 0.5, 1), 'ls': '-'},
              {'color': (0.3, 0, 0.5, 1), 'ls': '-'},
              {'color': (0.3, 0, 0.5, 1), 'ls': '-'},
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.', 'label': "v prstech"},
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.'},
              {'color': (0.5, 0.5, 0.5, 1), 'ls': '-', 'label': "volně"},
              {'color': (0.5, 0.5, 0.5, 1), 'ls': '-'}]
    outname = 'fengold'
    main(pathprefix, files, styles, outname, index)

#!/usr/bin/env python3
import sys
sys.path.append('/home/yan/playground/python/vswrview')
from vswrview_complex import main



if __name__ == "__main__":
    index = "b)"
    pathprefix = '~/documents/pmr/measurements/'+\
                 '2021_01_09_nagoya_chinagoya_retevis_pruty/rawdata/'
    files = ['na771_1_h.s1p', 'na771_1_h2.s1p', 'chinag5_t1.s1p', 'chinag5_t2.s1p']
    styles = [{'color': (0.5, 0.5, 0.5, 1), 'ls': '-', 'label': "orig#1"},
              {'color': (0.5, 0.5, 0.5, 1), 'ls': '-'},
              {'color': (0.3, 0, 0.5, 1), 'ls': '-', 'label': "copy#5"},
              {'color': (0.3, 0, 0.5, 1), 'ls': '-'}]

    outname = 'copyvsreal'
    main(pathprefix, files, styles, outname, index)

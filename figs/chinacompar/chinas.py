#!/usr/bin/env python3
import sys
sys.path.append('/home/yan/playground/python/vswrview')
from vswrview_complex import main



if __name__ == "__main__":
    index = "a)"
    pathprefix = '~/documents/pmr/measurements/'+\
                 '2021_01_09_nagoya_chinagoya_retevis_pruty/rawdata/chinag'
    files = ['1_t1.s1p', '1_t2.s1p', '2_t1.s1p', '2_t2.s1p','3_t1.s1p', '3_t2.s1p','4_t1.s1p', '4_t2.s1p','5_t1.s1p', '5_t2.s1p',]
    styles = [{'color': (0.3, 0, 0.5, 1), 'ls': '-', 'label': "#1"},
              {'color': (0.3, 0, 0.5, 1), 'ls': '-'}, 
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.', 'label': "#2"},
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.'},
              {'color': (0, 0.7, 0.5, 1), 'ls': '--', 'label': "#3"},
              {'color': (0, 0.7, 0.5, 1), 'ls': '--'},
              {'color': (0.5, 0.5, 0.5, 1), 'ls': '-', 'label': "#4"},
              {'color': (0.5, 0.5, 0.5, 1), 'ls': '-'},
              {'color': (0.5, 0.7, 0.9, 1), 'ls': '-', 'label': "#5"},
              {'color': (0.5, 0.7, 0.9, 1), 'ls': '-'}]
    outname = 'chincompar'
    main(pathprefix, files, styles, outname, index)

#!/usr/bin/env python3
import sys
sys.path.append('/home/yan/playground/python/vswrview')
from vswrview_complex import main



if __name__ == "__main__":
    index = "a)"
    pathprefix = '~/documents/pmr/measurements/'+\
                 '2021_01_09_nagoya_chinagoya_retevis_pruty/rawdata/'
    files = ['1_t1.s1p', '1_t2.s1p', '2_t1.s1p', '2_t2.s1p','3_t1.s1p', '3_t2.s1p','4_t1.s1p', '4_t2.s1p','5_t1.s1p', '5_t2.s1p',]
    styles = [{'color': (0.3, 0, 0.5, 1), 'ls': '-', 'label': "copy"},
              {'color': (0.3, 0, 0.5, 1), 'ls': '-'}, 
              {'color': (0.5, 0.5, 0.5, 1), 'ls': '-', 'label': "orig"},
              {'color': (0.5, 0.5, 0.5, 1), 'ls': '-'}]
    outname = 'chincompar'
    main(pathprefix, files, styles, outname, index)

#!/usr/bin/env python3
import sys
sys.path.append('/home/yan/playground/python/vswrview')
from vswrview_complex import main



if __name__ == "__main__":
    index = "a"
    pathprefix = '~/documents/pmr/measurements/'+\
                 '2021_01_30_reduct_antennas/clanek_git/rawdata_new/'
    files = ['771nag1_t1.s1p','771nag1_t2.s1p','771nag1_l1.s1p','771nag1_l2.s1p','771nag1_f1.s1p','771nag1_f2.s1p']
    styles = [{'color': (0.3, 0, 0.5, 1), 'ls': '-', 'label': "natěsno"},
              {'color': (0.3, 0, 0.5, 1), 'ls': '-'}, 
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.', 'label': "v ruce"},
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.'},
              {'color': (0.5, 0.5, 0.5, 1), 'ls': '-', 'label': "volně"},
              {'color': (0.5, 0.5, 0.5, 1), 'ls': '-'}]
    outname = 'uchop_771'
    main(pathprefix, files, styles, outname, index)

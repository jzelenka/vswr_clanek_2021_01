#!/usr/bin/env python3
import sys
sys.path.append('/home/yan/playground/python/vswrview')
from vswrview_complex import main



if __name__ == "__main__":
    index = "a)"
    pathprefix = '~/documents/pmr/measurements/'+\
                 '2021_01_30_reduct_antennas/clanek_git/rawdata_new_redukce/'
    files = ['myload_ref.s1p', 'dvojredukce_load_ref.s1p']
    styles = [{'color': (0.3, 0, 0.5, 1), 'ls': '-', 'label': "reference"},
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.', 'label': "reference + redukce"}]
    outname = 'swr_redukce'
    main(pathprefix, files, styles, outname, index)

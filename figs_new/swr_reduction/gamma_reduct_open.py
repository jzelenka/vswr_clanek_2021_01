#!/usr/bin/env python3
import sys
from matplotlib.backends.backend_qt5agg import\
        FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from PyQt5 import QtGui
from PyQt5 import QtWidgets
from PyQt5 import QtCore
import numpy as np
import pandas as pd
import sys
sys.path.append('/home/yan/playground/python/vswrview')

def load_file(fn):
    a = pd.read_table(fn, delim_whitespace=True, skiprows=3, header=None,
                      names=("freq (MHz)", "Γ_real", "Γ_imag"))
    freqs = np.asarray(a).T[0]
    gammas = np.linalg.norm(np.asarray(a).T[1:], axis=0)
    return freqs, gammas


def populate(datas, plot, style):
    #plot.plot(datas[0], datas[1], color=(0,0.9,0,1))
    plot.plot(datas[0], datas[1], linewidth=0.8, **style)
    return 0


def main(pathprefix, files, styles, outname, index):
    app = QtWidgets.QApplication(sys.argv)
    graph = Figure(figsize=(5,2), dpi=100, facecolor="None",
            constrained_layout=True)
    spectra = graph.subplots(1,2, sharey=True, subplot_kw={"facecolor": (1, 1, 1, 0)})
    graph.set_constrained_layout_pads(wspace = -0.08)
    canvas = FigureCanvas(graph)
    canvas.setStyleSheet("background-color:transparent;")
    canvas.setAutoFillBackground(False)
    spectra[0].set_xlabel("wavelength(MHz)", x=1)
    spectra[0].set_ylabel("Γ (reflekance)")
    spectra[0].set_xticks(np.linspace(100,200,6))
    spectra[0].set_xlim(130,205)
    spectra[0].spines['right'].set_visible(False)
    spectra[0].set_yticks(np.linspace(0.8,1.2,3))
    spectra[1].set_xticks(np.linspace(400,500,6))
    spectra[1].set_xlim(415,500)
    spectra[1].spines['left'].set_visible(False)
    spectra[1].tick_params(axis='y',left=False)
    for spectrum in spectra:
        spectrum.set_ylim(0.8,1.2)
        spectrum.minorticks_on()
        spectrum.grid(True)
        spectrum.grid(True, 'minor', linewidth='0.2')
    spectra[1].tick_params(axis='y', which='both', left=False)

    #stolen from: https://matplotlib.org/3.3.3/gallery/subplots_axes_and_figures/broken_axis.html
    d = 8  # proportion of vertical to horizontal extent of the slanted line
    kwargs = dict(marker=[(-1, -d), (1, d)], markersize=12,
              linestyle="none", color='k', mec='k', mew=1, clip_on=False)
    spectra[0].plot([1, 1], [0, 1], transform=spectra[0].transAxes, **kwargs)
    spectra[1].plot([0, 0], [0, 1], transform=spectra[1].transAxes, **kwargs)

    main_window = QtWidgets.QMainWindow(windowTitle="VSWR view")
    for name, style in zip(files, styles):
        datas = load_file(pathprefix+name)
        for spectrum in spectra:
            populate(datas, spectrum, style)
    spectra[1].legend(loc=4)
    spectra[0].text(0, 0.975, index, va="top",
               transform=graph.transFigure, in_layout=False)

    graph.savefig(outname+".jpg", dpi=300)
    graph.savefig(outname+".png", dpi=300)
    pixmap=QtGui.QPixmap(outname+".png")
    label = QtWidgets.QLabel(alignment=QtCore.Qt.AlignCenter)
    main_window.setCentralWidget(label)
    main_window.resizeEvent = lambda x: label.setPixmap(
            pixmap.scaled(x.size(), QtCore.Qt.KeepAspectRatio,
                          QtCore.Qt.SmoothTransformation))
    main_window.show()
    sys.exit(app.exec_())
    return


if __name__ == "__main__":
    index = "b)"
    pathprefix = '~/documents/pmr/measurements/'+\
                 '2021_01_30_reduct_antennas/clanek_git/rawdata_new_redukce/'
    files = ['myopen_ref.s1p', 'dvojredukce_open_ref.s1p']
    styles = [{'color': (0.3, 0, 0.5, 1), 'ls': '-', 'label': "ref_open"},
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.', 'label': "ref_open + redukce"}]
    outname = 'open'
    main(pathprefix, files, styles, outname, index)

#!/usr/bin/env python3
import sys
sys.path.append('/home/yan/playground/python/vswrview')
from vswrview_complex import main



if __name__ == "__main__":
    index = "b)"
    pathprefix = '~/documents/pmr/measurements/'+\
                 '2021_01_30_reduct_antennas/clanek_git/rawdata_new/'
    files = ['chinag1_f1.s1p', 'chinag1_f2.s1p', 'chinag2_f1.s1p', 'chinag2_f2.s1p', 'chinag3_f1.s1p', 'chinag3_f2.s1p', 'chinag4_f1.s1p', 'chinag4_f2.s1p', 'chinag5_f1.s1p', 'chinag5_f2.s1p', '771nag1_f1.s1p', '771nag1_f2.s1p', '771nag2_f1.s1p', '771nag2_f2.s1p']
    styles = [{'color': (0.3, 0, 0.5, 1), 'ls': '-', 'label': "771_kopie"},
              {'color': (0.3, 0, 0.5, 1), 'ls': '-'}, 
              {'color': (0.3, 0, 0.5, 1), 'ls': '-'}, 
              {'color': (0.3, 0, 0.5, 1), 'ls': '-'}, 
              {'color': (0.3, 0, 0.5, 1), 'ls': '-'}, 
              {'color': (0.3, 0, 0.5, 1), 'ls': '-'}, 
              {'color': (0.3, 0, 0.5, 1), 'ls': '-'}, 
              {'color': (0.3, 0, 0.5, 1), 'ls': '-'}, 
              {'color': (0.3, 0, 0.5, 1), 'ls': '-'}, 
              {'color': (0.3, 0, 0.5, 1), 'ls': '-'}, 
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.', 'label': "771_originál"},
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.'},
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.'},
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.'},
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.'},
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.'},
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.'},
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.'},
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.'},
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.'}]
    outname = 'chinag_vs_nag'
    main(pathprefix, files, styles, outname, index)

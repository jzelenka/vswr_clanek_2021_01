#!/usr/bin/env python3
import sys
sys.path.append('/home/yan/playground/python/vswrview')
from vswrview_complex import main



if __name__ == "__main__":
    index = "a)"
    pathprefix = '~/documents/pmr/measurements/'+\
                 '2021_01_30_reduct_antennas/clanek_git/rawdata_new/chinag'
    files = ['1_t1.s1p', '1_t2.s1p','2_t1.s1p','2_t2.s1p','3_t1.s1p','3_t2.s1p','4_t1.s1p','4_t2.s1p','5_t1.s1p','5_t2.s1p','1_l1_sometimesdouble.s1p','1_l2.s1p','2_l1.s1p','2_l2.s1p','3_l1.s1p','3_l2.s1p','4_l1.s1p','4_l2_varian.s1p','5_l1.s1p','5_l2.s1p', '1_f1.s1p','1_f2.s1p', '2_f1.s1p','2_f2.s1p','3_f1.s1p','3_f2.s1p','4_f1.s1p','4_f2.s1p', '5_f1.s1p','5_f2.s1p']
    styles = [{'color': (0.3, 0, 0.5, 1), 'ls': '-', 'label': "natěsno"},
              {'color': (0.3, 0, 0.5, 1), 'ls': '-'}, 
              {'color': (0.3, 0, 0.5, 1), 'ls': '-'}, 
              {'color': (0.3, 0, 0.5, 1), 'ls': '-'}, 
              {'color': (0.3, 0, 0.5, 1), 'ls': '-'}, 
              {'color': (0.3, 0, 0.5, 1), 'ls': '-'}, 
              {'color': (0.3, 0, 0.5, 1), 'ls': '-'}, 
              {'color': (0.3, 0, 0.5, 1), 'ls': '-'}, 
              {'color': (0.3, 0, 0.5, 1), 'ls': '-'}, 
              {'color': (0.3, 0, 0.5, 1), 'ls': '-'}, 
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.', 'label': "v ruce"},
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.'},
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.'},
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.'},
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.'},
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.'},
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.'},
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.'},
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.'},
              {'color': (0.9, 0.6, 0, 1), 'ls': '-.'},
              {'color': (0.5, 0.5, 0.5, 1), 'ls': '-', 'label': "volně"},
              {'color': (0.5, 0.5, 0.5, 1), 'ls': '-'},
              {'color': (0.5, 0.5, 0.5, 1), 'ls': '-'},
              {'color': (0.5, 0.5, 0.5, 1), 'ls': '-'},
              {'color': (0.5, 0.5, 0.5, 1), 'ls': '-'},
              {'color': (0.5, 0.5, 0.5, 1), 'ls': '-'},
              {'color': (0.5, 0.5, 0.5, 1), 'ls': '-'},
              {'color': (0.5, 0.5, 0.5, 1), 'ls': '-'},
              {'color': (0.5, 0.5, 0.5, 1), 'ls': '-'},
              {'color': (0.5, 0.5, 0.5, 1), 'ls': '-'}]
    outname = 'chinag_compar'
    main(pathprefix, files, styles, outname, index)
